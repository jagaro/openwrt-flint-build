# Build OpenWrt for GL.iNet Flint (GL-AX1800)

This is an OpenWrt build script for the
[GL.iNet Flint (GL-AX1800)](https://www.gl-inet.com/products/gl-ax1800/).
Builds with installable images can be found at:

> https://builds.dayam.org/openwrt/flint/

## Usage

```sh
git clone https://gitlab.com/jagaro/openwrt-flint-build.git
cd openwrt-flint-build
./build.sh
```

_Note: This is a full build (all packages) and currently takes over 5 hours on a
6-CPU Linode (`g6-standard-6`) and consumes about 200GB._

## References

- https://github.com/solidus1983/openwrt
- https://github.com/JiaY-shi/openwrt
- https://github.com/quic/qca-sdk-nss-fw
- https://forum.openwrt.org/t/gl-inet-ax1800-new-router-openwrt-support/105163
- https://openwrt.org/docs/guide-developer/toolchain/use-buildsystem

- Target System: Qualcomm Atheros 802.11ax WiSoC-s
- Subtarget: Qualcomm Atheros IPQ60xx
- Target Profile: GL-iNet GL-AX1800
