#!/bin/bash
#
# Script to build OpenWrt for GL.iNet Flint (GL-AX1800) on Debian 11+.

set -euo pipefail

cd "$(realpath -m "${BASH_SOURCE[0]}/..")"

REPOSITORY_URL="https://github.com/JiaY-shi/openwrt.git"
REPOSITORY_BRANCH="ipq60xx-devel"
PUBLIC_URL="https://builds.dayam.org/openwrt/flint"

# Install dependencies
sudo apt-get update
sudo apt-get install -y build-essential clang flex bison g++ gawk \
  gcc-multilib g++-multilib gettext git libncurses-dev libssl-dev \
  python3-distutils rsync unzip zlib1g-dev file wget \
  subversion

# Clone openwrt repository
if [ -d openwrt ]; then
  (cd openwrt && git pull)
else
  git clone --depth 1 -b "$REPOSITORY_BRANCH" "$REPOSITORY_URL" openwrt
fi

cd openwrt

# Install feeds
scripts/feeds update -a
scripts/feeds install -a

# Apply patches
for p in ../patches/*.patch; do
  patch -p1 <"$p"
done

# Set version
BUILD_DATE="$(date -u +%Y%m%d%H%M%S)"
echo "$BUILD_DATE" >version

# Set config
cp ../config .config
sed -ri "s|^(CONFIG_VERSION_REPO)=.*\$|\\1=\"$PUBLIC_URL/$BUILD_DATE\"|" .config
make defconfig

# Build
BUILD_LOG=1 IGNORE_ERRORS=1 make -j$(($(nproc)+1))

# Copy logs
tar -cz logs >bin/build-logs.tar.gz
[ -f logs/package/error.txt ] && cp logs/package/error.txt bin/error.txt

echo "Build complete!"
echo "Remember to upload the files..."
