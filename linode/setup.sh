#!/bin/bash
#
# Script to setup a Linode to build OpenWrt for GL.iNet Flint (GL-AX1800).

set -euo pipefail

SERVER_HOSTNAME=buildhost
NORMAL_USER=openwrt

sed -ri 's/^(root):[^:]*:(.*)$/\1:!:\2/' /etc/shadow

sed -ri 's/^PasswordAuthentication.*$/PasswordAuthentication no/g' \
  /etc/ssh/sshd_config
systemctl restart ssh

echo "$SERVER_HOSTNAME" >/etc/hostname
hostname -F /etc/hostname

cat >/etc/hosts <<EOF
127.0.0.1       localhost
127.0.1.1       $SERVER_HOSTNAME

# The following lines are desirable for IPv6 capable hosts
::1             localhost ip6-localhost ip6-loopback
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters
EOF

echo '%sudo ALL=(ALL) NOPASSWD: ALL' >/etc/sudoers.d/sudo-group-nopasswd

getent group "$NORMAL_USER" >/dev/null ||
  groupadd "$NORMAL_USER"
getent passwd "$NORMAL_USER" >/dev/null ||
  useradd -g "$NORMAL_USER" -m -s /bin/bash "$NORMAL_USER"
usermod -G sudo "$NORMAL_USER"

mkdir -p "/home/$NORMAL_USER/.ssh"
cp /root/.ssh/authorized_keys "/home/$NORMAL_USER/.ssh/authorized_keys"
chown -R "$NORMAL_USER:$NORMAL_USER" "/home/$NORMAL_USER/.ssh"

apt-get update
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y

apt-get install -y git
