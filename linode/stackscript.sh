#!/bin/sh
#
# Linode StackScript to build OpenWrt for GL.iNet Flint (GL-AX1800).

(
  wget -qO- https://gitlab.com/jagaro/openwrt-flint-build/-/raw/main/linode/setup.sh | bash

  sudo -u openwrt -i bash <<'EOF'
    cd $HOME
    git clone https://gitlab.com/jagaro/openwrt-flint-build.git
    cd openwrt-flint-build
    ./build.sh
EOF

) >/var/log/stackscript.log 2>&1
