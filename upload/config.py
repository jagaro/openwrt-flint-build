#!/usr/bin/env python3

from pathlib import Path

S3_BUCKET = "builds.dayam.org"
S3_REGION = "us-lax-1"
S3_HOST = "linodeobjects.com"

REMOTE_BUILD_BASE_DIR = "/openwrt/flint"
REMOTE_BUILD_TARGET_SUBDIR = "targets/qualcommax/ipq60xx"

SOURCE_DIR = Path(__file__).resolve().parent

BUILD_DIR = SOURCE_DIR.parent / "openwrt" / "bin"
BUILD_VERSION_PATH = SOURCE_DIR.parent / "openwrt" / "version"

try:
    BUILD_VERSION = BUILD_VERSION_PATH.open(encoding="utf-8").read().strip()
except:
    BUILD_VERSION = "unknown"

S3CMD_CONFIG_PATH = SOURCE_DIR / "s3cmd-config"

if __name__ == "__main__":
    import shlex
    for key, value in sorted(locals().copy().items()):
        if key.isupper():
            print(key + "=" + shlex.quote(str(value)))
