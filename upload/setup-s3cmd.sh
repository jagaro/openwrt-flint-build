#!/bin/bash

set -euo pipefail

source <("$(realpath -m "${BASH_SOURCE[0]}/..")/config.py")

echo "Installing s3cmd..."
sudo apt-get install -qqy s3cmd

if [ -s "$S3CMD_CONFIG_PATH" ]; then
  echo >/dev/stderr "$S3CMD_CONFIG_PATH: exists, bailing..."
  exit 0
fi

echo
read -p "[ S3_ACCESS_KEY ]: " S3_ACCESS_KEY
read -p "[ S3_SECRET_KEY ]: " S3_SECRET_KEY

touch "$S3CMD_CONFIG_PATH"
chmod 600 "$S3CMD_CONFIG_PATH"
cat >"$S3CMD_CONFIG_PATH" <<EOF
[default]
access_key = $S3_ACCESS_KEY
secret_key = $S3_SECRET_KEY
check_ssl_certificate = True
check_ssl_hostname = True
host_base = $S3_REGION.$S3_HOST
host_bucket = %(bucket)s.$S3_REGION.$S3_HOST
public_url_use_https = True
use_https = True
website_endpoint = https://%(bucket)s.$S3_REGION.$S3_HOST/
website_error = error.html
website_index = index.html
EOF

echo
echo "$S3CMD_CONFIG_PATH: generated"
