#!/bin/bash

set -euo pipefail

source <("$(realpath -m "${BASH_SOURCE[0]}/..")/config.py")

if [ $# -gt 0 ]; then
  BUILD_VERSION="$1"
fi

S3_REMOTE_BUILD_DIR="s3://$S3_BUCKET$REMOTE_BUILD_BASE_DIR/$BUILD_VERSION"

echo "Syncing $BUILD_DIR/ -> $S3_REMOTE_BUILD_DIR/"
echo

s3cmd() {
  command s3cmd -c "$S3CMD_CONFIG_PATH" "$@"
}

s3cmd mb "s3://$S3_BUCKET"

s3cmd \
  --ws-index=index.html \
  --ws-error=404.html \
  ws-create "s3://$S3_BUCKET"

s3cmd \
  --acl-public \
  --no-mime-magic \
  --follow-symlinks \
  sync "$BUILD_DIR/" "$S3_REMOTE_BUILD_DIR/"

exit 0
