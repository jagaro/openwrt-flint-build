#!/usr/bin/env python3

from datetime import datetime
from html import escape as e
import mimetypes
from pathlib import Path
import re
from string import Template
import subprocess
import sys
import tempfile
from urllib.parse import quote

from config import *


PUBLIC_DIR = SOURCE_DIR / "public"
AUTOINDEX_TEMPLATE_PATH = PUBLIC_DIR / "autoindex.html"
INDEX_TEMPLATE_PATH = PUBLIC_DIR / "index.html"


def s3cmd(*args):
    args = ["s3cmd", "-c", str(S3CMD_CONFIG_PATH)] + list(args)
    print("$ " + " ".join(args))
    process = subprocess.run(args, capture_output=True, text=True)
    return process.stdout


def upload_file(remote_path, data):
    mime_type = mimetypes.guess_type(remote_path)[0] or 'application/octet-stream'
    with tempfile.NamedTemporaryFile(delete=False) as f:
        f.write(data)
        f.close()
        s3cmd("--acl-public", "-m", mime_type, "put", f.name, f"s3://{S3_BUCKET}{remote_path}")


def get_remote_files(remote_dir):
    output = s3cmd("ls", "-lH", f"s3://{S3_BUCKET}{remote_dir}/")
    files = []
    for line in output.splitlines():
        parts = line.split()
        if parts[0] == "DIR":
            kind = "dir"
            s3_path = parts[1]
            size = ""
            date = ""
        else:
            kind = "file"
            s3_path = parts[5]
            size = parts[2]
            date = parts[0] + " " + parts[1]
        path = re.sub(r"^s3://[^/]+", "", s3_path).rstrip("/")
        files.append( (kind, path, size, date) )
    files.sort()
    return files


def upload_index_html():
    builds = []
    for kind, remote_path, size, date in get_remote_files(REMOTE_BUILD_BASE_DIR):
        if kind != "dir":
            continue
        filename = remote_path.split("/")[-1]
        build_dir = quote(f"{REMOTE_BUILD_BASE_DIR}/{filename}/{REMOTE_BUILD_TARGET_SUBDIR}/")
        dt = datetime.strptime(filename, "%Y%m%d%H%M%S")
        builds.append(f"<li><a href=\"{build_dir}\">{str(dt)}</a></li>")
    builds_html = "\n".join(builds)
    template = Template(INDEX_TEMPLATE_PATH.open(encoding="utf-8").read())
    html = template.substitute(builds_html=builds_html)
    upload_file("/index.html", html.encode("utf-8"))


def upload_autoindex_html(remote_dir, files):
    lines = []
    for kind, path, size, date in files:
        filename = path.split("/")[-1]
        if filename == "index.html":
            continue
        url = quote(filename)
        if kind == "dir":
            url += "/"
            icon = "📁"
            hint = ""
        else:
            icon = "📄"
            hint = " download"
        lines.append(
            "<tr>" +
            f"<td><a href=\"{e(url)}\"{hint}>{icon}</a></td>" +
            f"<td><a href=\"{e(url)}\"{hint}>{e(filename)}</a></td>" +
            f"<td>{size}</td>" +
            f"<td>{e(date)}</td>" +
            "</tr>"
        )
    autoindex_html = "\n".join(lines)
    template = Template(AUTOINDEX_TEMPLATE_PATH.open(encoding="utf-8").read())
    html = template.substitute(path=e(remote_dir), autoindex_html=autoindex_html)
    upload_file(remote_dir + "/index.html", html.encode("utf-8"))


def upload_autoindex_htmls(remote_dir):
    files = get_remote_files(remote_dir)
    upload_autoindex_html(remote_dir, files)
    for kind, file_path, size, date in files:
        if kind == "dir":
            upload_autoindex_htmls(file_path)


def upload_public_files():
    for file in PUBLIC_DIR.glob('*'):
        if file in (AUTOINDEX_TEMPLATE_PATH, INDEX_TEMPLATE_PATH):
            continue
        data = file.open("rb").read()
        upload_file(f"/{file.name}", data)


if __name__ == "__main__":
    upload_public_files()
    upload_index_html()
    start_dir = REMOTE_BUILD_BASE_DIR
    if len(sys.argv) > 1:
        start_dir += "/" + sys.argv[1].rstrip('/')
    upload_autoindex_htmls(start_dir)
