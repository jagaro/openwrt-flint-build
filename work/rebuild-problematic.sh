#!/bin/sh
#
# Cleanup and rebuild problematic packages.

cd "$(realpath -m "${BASH_SOURCE[0]}/..")"
cd ../openwrt

for i in luci packages routing telephony; do
  test -d feeds/$i && (
    cd feeds/$i
    git reset --hard
    git clean -fdx
  )
done

for p in ../patches/*.patch; do
  patch -p1 <"$p"
done

rm -rf logs

BUILD_LOG=1 make -j$(($(nproc)+1)) \
  package/libs/libxml2/{clean,compile} \
  package/feeds/packages/xmlrpc-c/{clean,compile} \
  package/kernel/ath10k-ct/{clean,compile} \
  package/feeds/packages/apache/{clean,compile} \
  package/feeds/routing/cjdns/{clean,compile} \
  package/feeds/packages/hs20/{clean,compile} \
  package/feeds/packages/libiio/{clean,compile} \
  package/feeds/packages/libsoup/{clean,compile} \
  package/feeds/packages/mariadb/{clean,compile} \
  package/feeds/packages/openconnect/{clean,compile} \
  package/feeds/packages/openthread-br/{clean,compile} \
  package/feeds/packages/php8/{clean,compile} \
  package/feeds/packages/rtorrent/{clean,compile} \
  package/feeds/telephony/rtpengine/{clean,compile}
